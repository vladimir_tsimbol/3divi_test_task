import numpy as np
import math
import sys
import os
import re


def generate(P=0.5, width=500, height=500):
    image = np.zeros((width, height), dtype=int)
    while True:
        points = np.column_stack((np.random.uniform(0, width, size=3), np.random.uniform(0, height, size=3)))
        if check_triangle(points):
            draw_line(image, points[0], points[1])
            draw_line(image, points[1], points[2])
            draw_line(image, points[0], points[2])
            return noise(image, P), points


def restore(image):
    # Отфильтровываем изображение
    width = image.shape[0]
    height = image.shape[1]
    points = [[x, y] for x in range(1, width - 1) for y in range(1, height - 1) if
              mask_filter(image[x - 1: x + 2, y - 1: y + 2])]
    # Параметры дискретности пространства Хафа
    d_angle = 1
    d_r = 1
    # Строим пространство Хафа и заполняем его
    d = int(math.sqrt(image.shape[0] ** 2 + image.shape[1] ** 2))
    angles = np.arange(0, 181, d_angle)
    # Кешируем углы в радианах и значения косинусов и синусов
    theta = np.array([math.radians(x) for x in angles])
    sin_cos = np.array([(math.sin(x), math.cos(x)) for x in theta])
    # Заполняем пространство
    data = np.array([[[0, r, a] for a in angles] for r in range(-d, d, d_r)])
    R = [(int((x * sin_cos[i][1] + y * sin_cos[i][0] + d) / d_r), i) for i in range(sin_cos.shape[0]) for x, y in
         points]
    for r, i in R:
        data[r][i][0] += 1
    # Ищем максимумы
    hough_data = [(data[r][a][0], data[r][a][1], data[r][a][2]) for a in range(data.shape[1])
                  for r in range(data.shape[0]) if data[r][a][0] >= 2]
    local_max = sorted(hough_data, key=lambda x: x[0], reverse=True)
    data_length = len(local_max)
    for i in range(data_length):
        for j in range(i + 1, data_length):
            if math.fabs(local_max[i][2] - local_max[j][2]) >= 30:
                for k in range(j + 1, data_length):
                    if math.fabs(local_max[k][2] - local_max[i][2]) >= 30 and math.fabs(
                            local_max[k][2] - local_max[j][2]) >= 30:
                        ret, points = get_points([local_max[i], local_max[j], local_max[k]], width, height)
                        if ret:
                            if check_triangle(points):
                                return points
    return np.zeros((3, 2))


def mask_filter(points):
    if points[1, 1] > 0:
        if (254 <= points[1, 1] + points[1, 2] <= 255) or 254 <= points[1, 1] + points[2, 1] <= 255:
            return True
        elif points[1, 1] == points[1, 2] == 255 or points[1, 1] == points[2, 1] == 255:
            return True
    return False


def get_points(line_parametrs, width=500, height=500):
    theta = np.array([math.radians(x[2]) for x in line_parametrs])
    sin_cos = np.array([(math.sin(x), math.cos(x)) for x in theta])
    index = lambda x: x if x < 3 else 0
    y_points = np.array(
        [(line_parametrs[i][1] * sin_cos[index(i + 1)][1] - line_parametrs[index(i + 1)][1] * sin_cos[i][1]) /
         (sin_cos[i][0] * sin_cos[index(i + 1)][1] - sin_cos[index(i + 1)][0] * sin_cos[i][1]) for i in range(3)])
    x_points = np.array(
        [(line_parametrs[i][1] * sin_cos[index(i + 1)][0] - line_parametrs[index(i + 1)][1] * sin_cos[i][0]) /
         (sin_cos[i][1] * sin_cos[index(i + 1)][0] - sin_cos[index(i + 1)][1] * sin_cos[i][0]) for i in range(3)])
    if (np.amin(x_points) < 0 or np.amax(x_points) > width) or (np.amin(y_points) < 0 or np.amax(y_points) > height):
        return False, None
    else:
        return True, np.column_stack((x_points, y_points))


def check_triangle(points):
    # Вычисляем координаты векторов
    index = lambda x: x if x < 3 else 0
    vectors = np.array(
        [[points[index(i + 1)][0] - points[i][0], points[index(i + 1)][1] - points[i][1]] for i in range(3)])
    vectors_length = np.array([math.sqrt(x[0] ** 2 + x[1] ** 2) for x in vectors])
    if np.amin(vectors_length) >= 100:
        angles = np.array([
            math.degrees(math.acos((vectors_length[0] ** 2 + vectors_length[2] ** 2 - vectors_length[1] ** 2) /
                                   (2 * vectors_length[0] * vectors_length[2]))),
            math.degrees(math.acos((vectors_length[0] ** 2 + vectors_length[1] ** 2 - vectors_length[2] ** 2) /
                                   (2 * vectors_length[0] * vectors_length[1])))
        ])
        if np.amin(angles) >= 30 and 180 - np.sum(angles) >= 30:
            return True
    return False


def draw_line(image, point1, point2):
    x1 = int(point1[0])
    y1 = int(point1[1])
    x2 = int(point2[0])
    y2 = int(point2[1])
    # Определяемся с осью, вдоль которой пойдем
    mode = abs(y2 - y1) > abs(x2 - x1)
    if mode:
        x1, y1 = y1, x1
        x2, y2 = y2, x2
    if x2 < x1:
        x1, x2 = x2, x1
        y1, y2 = y2, y1
    # Вычисление градиента
    if x2 - x1 == 0:
        gradient = 1.0
    else:
        gradient = (y2 - y1) / (x2 - x1)
    # Лямбды
    fpart = lambda x: x - int(x)
    # Начальная точка
    x = round(x1)
    y = y1 + gradient * (x - x1)
    yi = y + gradient
    draw_point(image, mode, x, int(y), np.uint8((1 - fpart(y)) * 255))
    draw_point(image, mode, x, int(y) + 1, np.uint8(fpart(y) * 255))
    # Конечная точка
    x = round(x2)
    y = y2 + gradient * (x - x2)
    draw_point(image, mode, x, int(y), np.uint8((1 - fpart(y)) * 255))
    draw_point(image, mode, x, int(y) + 1, np.uint8(fpart(y) * 255))
    # Промежуточные точки
    for xi in range(round(x1) + 1, round(x2)):
        draw_point(image, mode, xi, int(yi), np.uint8((1 - fpart(yi)) * 255))
        draw_point(image, mode, xi, int(yi) + 1, np.uint8(fpart(yi) * 255))
        yi += gradient


def draw_point(image, mode, x, y, value):
    if mode:
        x, y = y, x
    x = min(image.shape[0] - 1, x)
    y = min(image.shape[1] - 1, y)
    image[x, y] = value


def noise(image, P=0.5):
    noise_array = np.random.randint(0, 255, size=(image.shape[0], image.shape[1]))
    P_array = np.random.uniform(0, 1, size=(image.shape[0], image.shape[1]))
    return np.where(P_array > P, image, noise_array)


def save_text_format_image(image, file_path='image.pgm'):
    with open(file_path, 'w') as file:
        file.write(f'P2\n{image.shape[0]} {image.shape[1]}\n255')
        for row in image:
            file.write('\n' + ' '.join(map(str, row)))


def load_text_format_image(file_path='image.pgm'):
    with open(file_path, 'r') as file:
        data = np.array([int(s) for s in file.read().split() if s.isdigit()])
        image = np.array(data[3:].reshape((data[0], data[1]))).astype(int)
        return image


def load_binary_format_image(file_path='image.pgm'):
    with open(file_path, 'rb') as file:
        buffer = file.read()
        header, width, height, maxval = re.search(
            b"(^P5\s(?:\s*#.*[\r\n])*"
            b"(\d+)\s(?:\s*#.*[\r\n])*"
            b"(\d+)\s(?:\s*#.*[\r\n])*"
            b"(\d+)\s(?:\s*#.*[\r\n]\s)*)", buffer).groups()
        return np.frombuffer(buffer,
                             dtype='u1' if int(maxval) < 256 else '>' + 'u2',
                             count=int(width) * int(height),
                             offset=len(header)
                             ).reshape((int(height), int(width))).astype(int)


def save_points(points, file_path='output.txt'):
    with open(file_path, 'w') as file:
        for point in points:
            file.write(f'{point[1]:.3f} {point[0]:.3f}\n')


if __name__ == "__main__":
    BASE_PATH = os.path.dirname(os.path.abspath(__file__))
    if len(sys.argv) == 3:
        if sys.argv[1] == '-generate':
            P = float(sys.argv[2])
            if 0 <= P <= 1:
                img, points = generate(P)
                save_text_format_image(img, os.path.join(BASE_PATH, 'image.pgm'))
            else:
                raise Exception('Error P-value')
        elif sys.argv[1] == '-restore':
            img = load_binary_format_image(os.path.join(BASE_PATH, sys.argv[2]))
            points = restore(img)
            save_points(points, os.path.join(BASE_PATH, 'output.txt'))
        else:
            raise Exception('Unknown command')
    else:
        raise Exception('More parameters required')
