import math
import numpy as np
import pandas as pd
import sys
import os
from solver import save_image, load_image, restore, generate, noise, draw_line
import matplotlib.pyplot as plt
import time


def metrics(generate_points, restore_points):
    d1 = np.min([math.sqrt((x - generate_points[0][0]) ** 2 + (y - generate_points[0][1]) ** 2) for x, y in restore_points])
    d2 = np.min([math.sqrt((x - generate_points[1][0]) ** 2 + (y - generate_points[1][1]) ** 2) for x, y in restore_points])
    d3 = np.min([math.sqrt((x - generate_points[2][0]) ** 2 + (y - generate_points[2][1]) ** 2) for x, y in restore_points])
    return np.round(np.max([d1, d2, d3]), 3)


def generate_special_triangle(P, points, width=500, height=500):
    image = np.zeros((width, height), dtype=int)
    draw_line(image, points[0], points[1])
    draw_line(image, points[1], points[2])
    draw_line(image, points[0], points[2])
    return noise(image, P)


def generate_dataset(dir, width=500, height=500):
    dP = 0.05
    image_count = 5
    file_names = []
    P_values = []
    x1 = []
    y1 = []
    x2 = []
    y2 = []
    x3 = []
    y3 = []
    # Clear directory
    files = os.listdir(dir)
    for f in files:
        os.remove(f"{dir}{f}")
    # Usual triangles
    for P in np.arange(0.0, 0.8, dP):
        for i in range(image_count):
            file_name = f"image{int(P * 100)}_{i}.pgm"
            img, points = generate(P, width, height)
            ret = save_image(img, f"{dir}{file_name}")
            if ret:
                file_names.append(file_name)
                P_values.append(P)
                points = np.round(points, 3)
                x1.append(points[0][0])
                x2.append(points[1][0])
                x3.append(points[2][0])
                y1.append(points[0][1])
                y2.append(points[1][1])
                y3.append(points[2][1])
            else:
                print(f"Error save {dir}{file_name}")
    # Save to dataset -> .csv
    df = pd.DataFrame({'file_name': file_names, 'P': P_values, 'x1': x1, 'y1': y1, 'x2': x2, 'y2': y2, 'x3': x3, 'y3': y3})
    df.to_csv(f"{dir}dataset.csv", sep=";", index=False)


def test(dataset_dir, statistic_dir):
    df = pd.read_csv(f"{dataset_dir}dataset.csv", sep=';')
    file_names = df['file_name']
    P_values = df['P']
    loss = []
    x1 = []
    x2 = []
    x3 = []
    y1 = []
    y2 = []
    y3 = []
    for i in np.arange(df.shape[0]):
        print(f'{file_names[i]}')
        start_time = time.time()
        ret, img = load_image(f"{dataset_dir}{file_names[i]}")
        if ret:
            ret, r_points = restore(img)
            print(time.time() - start_time)
            if ret:
                # Сравниваем результаты
                s_points = [[df['x1'][i], df['y1'][i]], [df['x2'][i], df['y2'][i]], [df['x3'][i], df['y3'][i]]]
                r_points = np.round(r_points, 3)
                x1.append(r_points[0][0])
                x2.append(r_points[1][0])
                x3.append(r_points[2][0])
                y1.append(r_points[0][1])
                y2.append(r_points[1][1])
                y3.append(r_points[2][1])
                loss.append(np.min([metrics(np.roll(s_points, i, axis=0), r_points) for i in range(3)]))
            else:
                loss.append(-1.0)
        else:
            print(f"Error load image {file_names[i]}")
    # Save to dataset -> .csv
    df = pd.DataFrame({'file_name': file_names, 'P': P_values, 'x1': x1, 'y1': y1, 'x2': x2, 'y2': y2, 'x3': x3, 'y3': y3, 'loss': loss})
    df.to_csv(f"{statistic_dir}statistcs.csv", sep=";", index=False)


def visualisation(statistic_dir):
    df = pd.read_csv(f"{statistic_dir}statistcs.csv", sep=';')
    fig, ax = plt.subplots(figsize=(16, 9))
    ax.set_xlabel('P')
    ax.set_ylabel('loss')
    ax.plot(df['P'], df['loss'], 'ro')
    plt.grid(True)
    fig.savefig(f'{statistic_dir}result.png')
    plt.close('all')


if __name__ == "__main__":
    BASE_PATH = os.path.dirname(__file__) + os.sep
    DATASET_PATH = BASE_PATH + 'dataset' + os.sep
    TEST_RESULT_PATH = BASE_PATH + 'statistic' + os.sep
    if len(sys.argv) == 2:
        if sys.argv[1] == '-generate':
            generate_dataset(DATASET_PATH)
        elif sys.argv[1] == '-test':
            test(DATASET_PATH, TEST_RESULT_PATH)
        elif sys.argv[1] == '-visualisation':
            visualisation(TEST_RESULT_PATH)
        else:
            print('Unknown command')
    else:
        print('Need more parametrs')
